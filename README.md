# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação - Introdução a Engenharia. 

|Nome| gitlab user|
|---|---|
|Rodolfo Koch|rodolfokoch|
|Vinicios Frezza|vinifrezza|
|Eduardo Giordani|EduGg|

# Documentação

A documentação do projeto pode ser acessada pelo link:

https://cursoseaulas.gitlab.io/mypage 

# Links Úteis

* [Tutorial HTML](http://pt-br.html.net/tutorials/html/)
* [Gnuplot](http://fiscomp.if.ufrgs.br/index.php/Gnuplot)